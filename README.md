## Especificaciones

1. Python -v : 3.8.1 
2. Entorno virtual: test
3. Nombre base de datos: eventos (editable en config.py)

---

## 	Inicio

1.a. Iniciar el entorno virtual
1.b. Instalar los paquetes usando el archivo **paquetes.txt**
2. Iniciar con py main.py o python main.py

