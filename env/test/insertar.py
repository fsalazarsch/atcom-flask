from models import db, Terremoto


def insertar_terremoto(fecha_inicio, fecha_fin, magnitud_min, magnitud_max, salida):
	t = Terremoto(fecha_inicio = fecha_inicio, fecha_fin = fecha_fin, magnitud_min = magnitud_min, magnitud_max = magnitud_max, salida = salida)
	db.session.add(t)
	db.session.commit()

