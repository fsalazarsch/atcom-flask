from flask import Flask, render_template, request, jsonify
from flask_wtf import CSRFProtect
from config import DevelopmentConfig
from models import db, Terremoto
from insertar import insertar_terremoto
import os, sys, json, datetime, requests


app = Flask(__name__)
app.config.from_object(DevelopmentConfig)

csrf = CSRFProtect(app)

url="https://earthquake.usgs.gov/fdsnws/event/1/query"

@app.errorhandler(404)
def page_not_found(e):
	return render_template("404.html"), 404

@app.route('/searchEarthquake', methods=['GET'])
def index():
	return render_template('index.html')

def formatfecha(milisec):
	x = datetime.datetime.fromtimestamp(milisec/1000)
	return x.strftime("%A, %B %d, %Y %H:%M:%S %p")

def formatfechains(milisec):
	x = datetime.datetime.fromtimestamp(milisec/1000)
	return x.strftime("%Y-%m-%d %H:%M:%S")

@app.route('/searchEarthquake/getEarthquakesByDates', methods=['GET'])
def bydates():

	minmagnitude = request.args.get('magnitudMinima', '')
	starttime= request.args.get('fechaInicio', '')
	endtime = request.args.get('fechFin', '')

	if minmagnitude == '':
		return '{ "mensaje":"Error, falta una variable", "var":"magnitudMinima"}'
	if starttime == '':
		return '{ "mensaje":"Error, falta una variable", "var":"fechaInicio"}'
	if endtime == '':
		return '{ "mensaje":"Error, falta una variable", "var":"fechFin"}'


	try:
		datetime.datetime.strptime(starttime, '%Y-%m-%d')
	except ValueError:
		return '{ "mensaje":"Error, formato no valido, debe ser YYYY-MM-DD o fecha invalida", "var":"fechaInicio"}'

	try:
		datetime.datetime.strptime(endtime, '%Y-%m-%d')
	except ValueError:
		return '{ "mensaje":"Error, formato no valido, debe ser YYYY-MM-DD o fecha invalida", "var":"fechaFin"}'

	else:
		#Datos de prueba
		#starttime = '2014-01-01'
		#endtime= '2014-01-02'
		#minmagnitude = 5


		maxmag = 0

		params = {'format': 'geojson', 'starttime': starttime, 'endtime': endtime, 'minmagnitude': minmagnitude}
		r = requests.get(url, params=params)

		resp = json.loads(r.text)

		total = []

		for feat in resp["features"]:
			aux = {}
			aux["mag"] = feat["properties"]["mag"]
			
			if( aux["mag"] > maxmag ):
				maxmag = aux["mag"]

			aux["place"] = feat["properties"]["place"]
			aux["time"] = formatfecha(feat["properties"]["time"])
			aux["updated"] = formatfecha(feat["properties"]["updated"])
			aux["alert"] = feat["properties"]["alert"]
			aux["status"] = feat["properties"]["status"]
			aux["tsunami"] = feat["properties"]["tsunami"]
			aux["magType"] = feat["properties"]["magType"]
			aux["type"] = feat["properties"]["type"]
			aux["title"] = feat["properties"]["title"]
			total.append(aux)

		insertar_terremoto(starttime, endtime, minmagnitude, maxmag, json.dumps(total))

		return json.dumps(total)

@app.route('/searchEarthquake/getEarthquakesByMagnitudes', methods=['GET'])
def bymagnitude():

	minmagnitude = request.args.get('magnitudMinima', '')
	maxmagnitude = request.args.get('magnitudMaxima', '')
	
	if minmagnitude == '':
		return '{ "mensaje":"Error, falta una variable", "var":"magnitudMinima"}'
	if maxmagnitude == '':
		return '{ "mensaje":"Error, falta una variable", "var":"magnitudMaxima"}'
	
	else:
		#Datos de prueba
		#starttime = '2014-01-01'
		#endtime= '2014-01-02'
		#minmagnitude = 5


		starttime = 9999999999999
		endtime= 0

		params = {'format': 'geojson', 'maxmagnitude': maxmagnitude, 'minmagnitude': minmagnitude}
		r = requests.get(url, params=params)

		resp = json.loads(r.text)

		total = []

		for feat in resp["features"]:
			aux = {}
			aux["mag"] = feat["properties"]["mag"]
			aux["place"] = feat["properties"]["place"]


			if( feat["properties"]["time"] < starttime ):
				starttime = feat["properties"]["time"]
			if( feat["properties"]["time"] > endtime ):
				endtime = feat["properties"]["time"]


			aux["time"] = formatfecha(feat["properties"]["time"])
			aux["updated"] = formatfecha(feat["properties"]["updated"])
			aux["alert"] = feat["properties"]["alert"]
			aux["status"] = feat["properties"]["status"]
			aux["tsunami"] = feat["properties"]["tsunami"]
			aux["magType"] = feat["properties"]["magType"]
			aux["type"] = feat["properties"]["type"]
			aux["title"] = feat["properties"]["title"]
			total.append(aux)

		insertar_terremoto(formatfechains(starttime), formatfechains(endtime), minmagnitude, maxmagnitude, json.dumps(total))

		return json.dumps(total)


if __name__ == '__main__':
	csrf.init_app(app)
	db.init_app(app)

	#migrate
	with app.app_context():
		db.create_all()

	app.run(host="0.0.0.0", port=8099)
