import os

class Config(object):
	SECRET_KEY = "iUZqQFHNntuy2AJHTMuS"

class DevelopmentConfig(Config):
	DEBUG = True
	#'mysql://root:password@localhost/flask'
	SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/eventos'
	SQLALCHEMY_TRACK_MODIFICATIONS = False