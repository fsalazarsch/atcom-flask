from flask_sqlalchemy import SQLAlchemy
import datetime


db = SQLAlchemy()

class Terremoto(db.Model):
	__tablename__ = 'earthquake'
	create_at = db.Column(db.Integer, primary_key=True)
	fecha_inicio = db.Column(db.DateTime, default=datetime.datetime.now)
	fecha_fin = db.Column(db.DateTime, default=datetime.datetime.now)
	magnitud_min = db.Column(db.Float)
	magnitud_max = db.Column(db.Float)
	salida = db.Column(db.Text)
	